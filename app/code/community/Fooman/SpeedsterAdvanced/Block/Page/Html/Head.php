<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com) (original implementation)
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz) (use of Minify Library)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/*
 * @author     Kristof Ringleff
 * @package    Fooman_SpeedsterAdvanced
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 */

class Fooman_SpeedsterAdvanced_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{
    const ASSET_TYPE_STATIC = 'static';
    const ASSET_TYPE_SKIN   = 'skin';
    const XML_PATH_LESS_ENABLED = 'dev/css/less_enabled';

    protected $_bundleItems = array(
        'static' => array(),
        'skin'   => array(),
    );

    public function setBundleItems($type, $files)
    {
        $files = (array)$files;
        if (stripos($type, 'skin') !== false) {
            $this->_bundleItems['skin'] = array_merge($this->_bundleItems['skin'], $files);
        } else {
            $this->_bundleItems['static'] = array_merge($this->_bundleItems['static'], $files);
        }
        return $this;
    }

    /**
     * Separate the files in bundle and non-bundle items
     *
     * By default, Magento separates by parameters ($params in the code) which includes
     * extra parameters to include in de html tag. This method adds an extra space to
     * the parameters for bundle items, causing them to be separated in bundle and non-
     * bundle items.
     *
     * @param array $items
     * @param string $type  One of 'skin' or 'static' (see ASSET_TYPE_xx constants)
     * @return array  The modified items array
     */
    protected function _separateByBundle($items, $type)
    {
        $newItems = array();
        foreach ($items as $params => $rows) {
            foreach ($rows as $key => $row) {
                $newParams = $params;
                if (in_array($row, $this->_bundleItems[$type])) {
                    $newParams .= ' ';
                }
                if (!isset($newItems[$newParams])) {
                    $newItems[$newParams] = array();
                }
                $newItems[$newParams][$key] = $row;
            }
        }
        return $newItems;
    }

    /**
     * Merge static and skin files of the same format into 1 set of HEAD directives or even into 1 directive
     *
     * Will attempt to merge into 1 directive, if merging callback is provided. In this case it will generate
     * filenames, rather than render urls.
     * The merger callback is responsible for checking whether files exist, merging them and giving result URL
     *
     * @param string   $format      - HTML element format for sprintf('<element src="%s"%s />', $src, $params)
     * @param array    $staticItems - array of relative names of static items to be grabbed from js/ folder
     * @param array    $skinItems   - array of relative names of skin items to be found in skins according to design config
     * @param callback $mergeCallback
     *
     * @return string
     */
    protected function &_prepareStaticAndSkinElements(
        $format, array $staticItems, array $skinItems, $mergeCallback = null
    )
    {
        $designPackage = Mage::getDesign();
        $baseJsUrl = Mage::getBaseUrl('js');
        $items = array();
        if ($mergeCallback && !is_callable($mergeCallback)) {
            $mergeCallback = null;
        }

        $staticItems = $this->_separateByBundle($staticItems, self::ASSET_TYPE_STATIC);
        $skinItems = $this->_separateByBundle($skinItems, self::ASSET_TYPE_SKIN);

        // get static files from the js folder, no need in lookups
        foreach ($staticItems as $params => $rows) {
            foreach ($rows as $name) {
                if ($mergeCallback) {
                    $items['static'][$params][] = Mage::getBaseDir() . DS . 'js' . DS . $name;
                } else {
                    $result = $baseJsUrl . $name;
                    $version = Mage::getStoreConfig(Fooman_SpeedsterAdvanced_Model_BuildSpeedster::XML_PATH_VERSION);
                    if ($version > '') {
                        if (preg_match('/(.*)\.(js)$/i', $result) || preg_match('/(.*)\.(css)$/i', $result)) {
                            $hasQuery = strpos($result, '?') !== false;
                            if ($hasQuery) {
                                $result .= '&v=' . $version;
                            } else {
                                $result .= '?v=' . $version;
                            }
                        }
                    }
                    $items['static'][$params][] = $result;
                }
            }
        }

        // lookup each file basing on current theme configuration
        foreach ($skinItems as $params => $rows) {
            foreach ($rows as $name) {
                $items['skin'][$params][] = $mergeCallback ? $designPackage->getFilename(
                    $name, array('_type' => 'skin')
                )
                    : $designPackage->getSkinUrl($name, array());
            }
        }

        $html = '';
        foreach ($items as $type) {
            foreach ($type as $params => $rows) {
                // attempt to merge
                $mergedUrl = false;
                foreach ($rows as $key => $row) {
                    if (strpos($row, Mage::getBaseDir('skin')) === 0 && !file_exists($row)) {
                        Mage::log('File to merge does not exist: ' . $row, Zend_Log::WARN);
                        unset($rows[$key]);
                    }
                }
                if ($mergeCallback) {
                    $mergedUrl = call_user_func($mergeCallback, $rows);
                }
                // render elements
                $params = trim($params);
                $params = $params ? ' ' . $params : '';
                if ($mergedUrl) {
                    $html .= sprintf($format, $mergedUrl, $params);
                } else {
                    foreach ($rows as $src) {
                        $newFormat = $format;
                        if (Mage::helper('speedsterAdvanced')->isLessEnabled()) {
                            if (preg_match('/\.less$/', $src)) {
                                $newFormat = str_replace('rel="stylesheet"', 'rel="stylesheet/less"', $format);
                            }
                        }
                        $html .= sprintf($newFormat, $src, $params);
                    }
                }
            }
        }
        return $html;
    }

    public function getCssJsHtml()
    {
        $lessJs = '';
        $shouldMergeCss = Mage::getStoreConfigFlag('dev/css/merge_css_files');
        if (!$shouldMergeCss && Mage::helper('speedsterAdvanced')->isLessEnabled()) {
            $lessJs = '
                <script type="text/javascript">
                var less = {
                    env: "development",
                    logLevel: 1,
                    async: false,
                    fileAsync: false,
                    poll: 1000,
                    dumpLineNumbers: "all",
                    relativeUrls: false
                };
                </script>
                <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.0/less.min.js"></script>
                ';
        }
        return parent::getCssJsHtml() . $lessJs;
    }

}
