<?php

/*
 * @package Minify
 * @author Stephen Clay <steve@mrclay.org>
 */

/*
 *
 * @author     Kristof Ringleff
 * @package    Fooman_SpeedsterAdvanced
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 */

require_once 'minify'.DS.'Minify'.DS.'CSS.php';

class Fooman_SpeedsterAdvanced_Model_Css extends Minify_CSS
{

    static public function minify($css, $options = array())
    {
        if (Mage::helper('speedsterAdvanced')->isLessEnabled()
            && isset($options['filename'])
            && preg_match('/\.less$/', $options['filename'])
        ) {
            try {
                // don't use LessPHP's autoloader, but use Varien_Autoloader
                // require_once 'Less/Autoloader.php';
                $baseName = basename($options['filename']);
                $baseDir  = rtrim(dirname($options['filename']), '/');
                Less_Autoloader::register();
                $oldCwd = getcwd();
                chdir($baseDir);

                $cacheDir = Mage::getBaseDir('var') . '/less_cache';
                if (!is_dir($cacheDir)) {
                    mkdir($cacheDir, 0777, false);
                }

                $parser = new Less_Parser();
                $parser->SetOption('relativeUrls', false);
                $parser->SetOption('cache_dir', $cacheDir);
                if (Mage::getIsDeveloperMode()) {
                    $parser->SetOption('sourceMap', true);
                }
                $parser->parseFile($baseName, $options['prependRelativePath']);
                $css = $parser->getCss();
                if (Mage::getIsDeveloperMode()) {
                    $css = str_replace('/*#', '/*!', $css);
                    $options['preserveComments'] = true;
                }
                chdir($oldCwd);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        if (Mage::getIsDeveloperMode()) {
            $css = str_replace('/*#', '/*!', $css);
            //return $css;
        }
        $css = parent::minify($css, $options);
        if (!empty($options['preserveComments'])) {
            $css = str_replace('/*!', '/*#', $css);
        }
        return $css;
    }

}