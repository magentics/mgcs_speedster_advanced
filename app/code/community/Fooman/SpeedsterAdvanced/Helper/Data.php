<?php

class Fooman_SpeedsterAdvanced_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_LESS_ENABLED = 'dev/css/less_enabled';

    public function isLessEnabled($store = null)
    {
        if (!isset($this->_lessEnabled)) {
            $this->_lessEnabled = false;
            if (Mage::getStoreConfigFlag(self::XML_PATH_LESS_ENABLED, $store)) {
                if (stream_resolve_include_path('Less/Autoloader.php')) {
                    $this->_lessEnabled = true;
                }
            }
        }
        return $this->_lessEnabled;
    }
}
